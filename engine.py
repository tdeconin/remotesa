from sqlalchemy.engine.strategies import strategies, EngineStrategy
from sqlalchemy.engine import base, threadlocal, url
from sqlalchemy import util
import sqlalchemy

import inspect
import xmlrpclib
import logging
import functools
import pickle
import sqlalchemy.ext.serializer as saser
import base64
from operator import attrgetter

import logging

def unimplemented(*args, **kwargs):
    raise NotImplementedError()

class WrappedEngineExecutor:
    def __init__(self,engine):
        self._engine = engine
        
    def execute(self,query, *multiparams, **params):
        return self._engine.execute(query, *multiparams, **params)    

class RemoteExecutor:
    def encode(self, orig, dump=pickle.dumps):
        p = dump (orig)
        return base64.b64encode(p)
    def decode(self, orig):
        p = base64.b64decode(orig)
        return pickle.loads(p)
    
    def execute(self,query, *multiparams, **params):
        q = self.encode(query, saser.dumps)
        m = self.encode(multiparams)
        p = self.encode(params)
        r = self.doExecuteRequest(q, m, p)
        return self.decode(r)
        
    def doExecuteRequest(self, q, m, p):
        unimplemented()

class XMLRPCExecutor(RemoteExecutor):
    def __init__ (self, ip, port):
        self._client = xmlrpclib.ServerProxy('http://%s:%d' % (ip, port))
        
    def doExecuteRequest(self, query, multiparams, params):
        return self._client.execute(query, multiparams, params)

    
class WrappedEngineStrategy (EngineStrategy):
    
    name = 'wrapped_engine'
    
    def __init__ (self ):
        strategies[ WrappedEngineStrategy.name ] = self
    
    
    def create(self, name_or_url, executor, **kwargs):
        # create url.URL object
        u = url.make_url(name_or_url)

        dialect_cls = u.get_dialect()

        dialect_args = {}
        # consume dialect arguments from kwargs
        for k in util.get_cls_kwargs(dialect_cls):
            if k in kwargs:
                dialect_args[k] = kwargs.pop(k)

        # create dialect
        dialect = dialect_cls(**dialect_args)

        return WrappedEngineStrategy.WrappedEngineConnection(dialect, executor)

   
    class WrappedEngineConnection(base.Connectable):
        def __init__(self, dialect, executor):
            self._dialect = dialect
            self.executor = executor
            self.compiler = self.create = self.drop = unimplemented
            l = logging.getLogger()
            self.debugEnabled = l.getEffectiveLevel() == logging.DEBUG
        
        def contextual_connect(self, *args, **kwargs):
            return self._id()
        
        def execute(self,*args,**kwargs):
            self._id()
            return self.executor.execute(*args,**kwargs)
                
        def engine (self, *args, **kwargs):
            return self._id()
        
        def begin(self, *args, **kwargs):
            return self._id()
        
        def execution_options(self, *args, **kwargs):
            return self._id()
        
        def rollback(self, *args, **kwargs):
            return self._id()
        
        def commit(self, *args, **kwargs):
            return self._id()
        
        def close(self, *args, **kwargs):
            return self._id()
        
        dialect = property(attrgetter('_dialect'))
        name = property(lambda s: s._dialect.name)

        def _id(self, *args, **kwargs):
            if( self.debugEnabled ):
                name = inspect.stack()[1][3]
                logging.debug("Executing %s", name )
            return self
                    
        def _run_visitor(self, visitorcallable, element, 
                                        connection=None, 
                                        **kwargs):
            kwargs['checkfirst'] = False
            visitorcallable(self.dialect, self,
                                **kwargs).traverse(element)

        
WrappedEngineStrategy()
