
from SimpleXMLRPCServer import SimpleXMLRPCServer
from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler

from sqlalchemy import create_engine

from service import SQLAlchemyService


# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

# Create server
server = SimpleXMLRPCServer(("localhost", 8000),
                            requestHandler=RequestHandler)
server.register_introspection_functions()

engine = create_engine('postgresql+psycopg2://root:rooter@127.0.0.1:5432/alchemytest', echo=True, execution_options={"stream_results":False})
service = SQLAlchemyService(engine)
server.register_instance(service)

# Run the server's main loop
server.serve_forever()
