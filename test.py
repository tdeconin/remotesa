from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, MetaData
from sqlalchemy.orm import sessionmaker, mapper
from mappings import *
from engine import *
import xmlrpclib

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

xmlRPC = XMLRPCExecutor("127.0.0.1", 8000)
engine = create_engine('postgresql://', strategy=WrappedEngineStrategy.name , executor = xmlRPC, echo=False)

metadata = MetaData(bind=engine)
updateMapper(metadata)
Session = sessionmaker(bind=engine)

session = Session()
ed_user = User('ed', 'Ed Jones', 'edspassword')

print session.new
session.add( ed_user )
session.commit()
print session.new

session.add_all([
     User('wendy', 'Wendy Williams', 'foobar'),
     User('mary', 'Mary Contrary', 'xxg527'),
     User('fred', 'Fred Flinstone', 'blah')])
print session.new

our_user = session.query(User).filter_by(name='ed').first()

print ed_user is our_user
print our_user 

session2 = Session()
visible_user = session2.query(User).filter_by(name='fred').all()
print visible_user
visible_user = session2.query(User).filter_by(name='fred').first()
print visible_user

q = session.query(User)\
    .filter( User.name.in_(['wendy', 'mary']))\
    .order_by(User.name)
print q.all()

print "For loop:P"
for instance in session.query(User).order_by(User.id): 
    print instance.name, instance.fullname


