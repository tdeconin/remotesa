def safe_copy(dest, src, attr):
    if hasattr(src, attr):
        setattr(dest, attr, getattr(src, attr))

class StrippedContext:
    def __init__(self,context):
        safe_copy (self, context, 'inserted_primary_key')
        safe_copy (self, context, 'prefetch_cols')
        safe_copy (self, context, 'postfetch_cols') 
        safe_copy (self, context, 'compiled_parameters')
    

class StrippedConnection:
    def __init__(self, conn):
        pass
    
    def _handle_dbapi_exception (cls,*args, **kwargs):
        pass
    
    def _safe_close_cursor(self, *args, **kwargs):
        pass
    
    def should_close_with_result(self, *args, **kwargs):
        pass
    
    def close(self,*args, **kwargs):
        pass 
    
class StrippedCursor:
    
    def __init__(self, cursor):
        if cursor is not None:
            self._records = cursor.fetchall()
            cursor.close()
        else:
            self._records = []
        
    def fetchall(self):
        return self._records